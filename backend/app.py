from flask import Flask, request, jsonify
from flask_restful import Api, Resource, reqparse, fields, marshal, abort, marshal_with
import copy
import json
import traceback
import logging

app = Flask(__name__)
api = Api(app)

users = [
    {
        "name": "Pavan",
        "threatLevel": "Demon",
        "rank": "B",
        "password": "abc"
    }, {
        "name": "Bob",
        "threatLevel": "Dragon",
        "rank": "A",
        "password": "123"
    }, {
        "name": "Kirby",
        "threatLevel": "God",
        "rank": "S",
        "password": "123"
    }
]

user_response = {
    "name": fields.String,
    "threatLevel": fields.String,
    "rank": fields.String
}

# The authentication checked here is basic without jwt.
# Actual implemention should use jwt validity instead.
def validateTokenAndGetUser():
    parser = reqparse.RequestParser()
    parser.add_argument('jwt_token', location='cookies')
    args = parser.parse_args()
    try:
        jwt_token = json.loads(args['jwt_token'])
        if 'userId' in jwt_token and 'password' in jwt_token:
            userId = jwt_token['userId']
            password = jwt_token['password']
            for user in users:
                if (userId == user["name"] and password == user["password"]):
                    return user
    except Exception as e:
        logging.error(traceback.format_exc())
    return None


class Users(Resource):
    @marshal_with(user_response)
    def get(self):
        user = validateTokenAndGetUser()
        if(user is not None):
            return users, 200
        else:
            return abort(401, message="Session doesn't exist or user is logged out")

# Add Secure=Strict for production
class Login(Resource):
    @marshal_with(user_response)
    def post(self):
        try:
            json_data = request.get_json(force=True)
            userId = json_data["userId"]
            password = json_data["password"]
            for user in users:
                if (userId == user["name"] and password == user["password"]):
                    headers = [
                        ('Set-Cookie', 'hello=sample non HttpOnly cookie;Domain=.localhost.com;SameSite=Strict;Path=/;'),
                        ('Set-Cookie', 'strict=sample SameSiteStrict cookie;Domain=.localhost.com;SameSite=Strict;Path=/'),
                        (
                            'Set-Cookie',
                            'jwt_token=' +
                            json.dumps({"userId": userId, "password": password}
                                       )+';Domain=.localhost.com;SameSite=Strict;Path=/;HttpOnly;'
                        )
                    ]
                    return user, 201, headers
        except ValueError:
            print('Decoding JSON has failed')
        return abort(401, message="Invalid Credentials")


class Logout(Resource):
    @marshal_with(user_response)
    def post(self):
        headers = [('Set-Cookie', 'hello=sample non HttpOnly cookie;Domain=.localhost.com;SameSite=Strict;Path=/;Max-Age=0;'),
                   ('Set-Cookie', 'strict=sample SameSiteStrict cookie;Domain=.localhost.com;SameSite=Strict;Path=/;Max-Age=0;SameSite=Strict'),
                   ('Set-Cookie', 'jwt_token=;Domain=.localhost.com;SameSite=Strict;Path=/;Max-Age=0;HttpOnly;')]
        return '', 201, headers


class CheckSession(Resource):
    @marshal_with(user_response)
    def get(self):
        user = validateTokenAndGetUser()
        if(user is not None):
            return user, 200
        else:
            return abort(401, message="Unauthorized")


# @app.after_request
# def after_request(response):
#     response.headers.add('Access-Control-Allow-Origin', 'http://localhost:8000')
#     response.headers.add('Access-Control-Allow-Headers',
#                          'Content-Type,Authorization,Set-Cookie')
#     response.headers.add('Access-Control-Allow-Credentials',
#                          'true')
#     response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
#     return response


api.add_resource(Users, "/api/users")
api.add_resource(Login, "/api/login")
api.add_resource(Logout, "/api/logout")
api.add_resource(CheckSession, "/api/checkSession")

app.run(debug=True)
