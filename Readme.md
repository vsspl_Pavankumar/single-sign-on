#### Creating virtual environment for Python
`virtualenv -p /c/Users/Pavan/AppData/Local/Programs/Python/Python39/python.exe venv`

#### Activate python virtual env:
`source c:/Users/Pavan/source/misc/sso/venv/Scripts/activate`

#### Install dependencies:
`pip install -r backend/requirements.txt`

### Summary of Setup to understand the Architecture of this Project
1. Components involved: backend server for API, frontend server for UI, ip configuration for application related subdomain setup.  
2. backend server is python flask_restful based on the flask server for python.  
   frontend server is a http directory server by python.  
   both of these run in different ports.  
3. Since the Application is about [domain, subdomain] testing, nginx is used to route both ui and backend to single port.  
4. All domains and subdomains run the same website for now for testing purpose. To do that hosts file is edited to route all subdomains to the same port.
5. Looking into app.py for existing logins, there is no registration feature.

#### Starting the server:  
`python -m http.server -d ui & python backend/app.py`  

#### Sub domain and domain local setup
   -add this rule to etc hosts  
    `127.0.0.1 localhost localhost.com sub.localhost.com`  
  
   -run nginx server with the specified configuration attached in this project root  
  
   -Test the Single Sign on for sub.localhost.com and localhost.com  
  
   -The .com sufix is important. It enables sharing or cookies that are set to Domain=.localhost.com. Cookies won't be shared otherwise.  
  
   -In the production environment, it's up to you to either redirect the user to login page or show login as an overlay after check session response is returned.  
  
   -In Production environment also add the Secure=Strict attribute to the Cookies too. Also add Double Submit Cookie Techinique to make it more secure. Details about it are in the document google doc further below.  
   
#### Nginx Commands for Windows (and Linux):
   https://nginx.org/en/docs/windows.html

## To understand the Authentication flow look at this flow diagram
 https://app.diagrams.net/#G1tTSmvWSwMe21nMY6FPFXLEoq0Q-2zBDG

## More on the Design decisions and Implementation details in this doc 
 https://docs.google.com/document/d/1F-VWbp-haM3PJNpwumsVINx6nNwzODuy-4M23xyIWg8/edit